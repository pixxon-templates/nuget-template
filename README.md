# nuget-template

This project contains a template to create a nuget package, version it automatically and publish it into the Gitlab package registry.

## jobs

### build_package

Using the `mcr.microsoft.com/dotnet/sdk:7.0.401-alpine3.18` builds the example class library. Validates the build upon merge requests.

### deploy_package

Packs the example library into a nuget package. Uses the tag for version numbering, creates symbol package in `snupkg` format. After authentication, uploads the package into the Gitlab registry.
