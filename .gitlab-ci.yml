workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG

prepare_version:
  stage: .pre
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image:
    name: psanetra/git-semver
    entrypoint: [""]
  script:
    - git log
    - echo "TAG=$(git-semver next)" | tee -a tag.env
  artifacts:
    reports:
      dotenv: tag.env

prepare_changelog:
  stage: .pre
  rules: 
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image:
    name: node:20-alpine3.18
    entrypoint: [""]
  before_script:
    - apk add git
    - npm install --global conventional-changelog-cli 
  script:
    - conventional-changelog -p angular | tail -n +2 | tee -a _CHANGELOG.md
  artifacts:
    paths:
      - _CHANGELOG.md

create_release:
  stage: .post
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  needs:
    - job: prepare_version
      artifacts: true
    - job: prepare_changelog
      artifacts: true
    - job: test_package
      artifacts: false
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Creating new release $TAG"
  release:
    name: 'Release $TAG'
    description: './_CHANGELOG.md'
    tag_name: '$TAG'
    ref: '$CI_COMMIT_SHA'

verify_commits:
  stage: .pre
  rules: 
    - if: $CI_MERGE_REQUEST_DIFF_BASE_SHA
  image:
    name: node:20-alpine3.18
    entrypoint: [""]
  before_script:
    - apk add git
    - npm install --global @commitlint/config-angular @commitlint/cli
  script:
    - commitlint --from $CI_MERGE_REQUEST_DIFF_BASE_SHA --extends @commitlint/config-angular --strict --verbose

build_package:
  stage: build
  needs: []
  image: mcr.microsoft.com/dotnet/sdk:7.0.401-alpine3.18
  script:
    - dotnet build

test_package:
  stage: test
  needs: 
    - job: build_package
  image: mcr.microsoft.com/dotnet/sdk:7.0.401-alpine3.18
  before_script:
  - export DOTNET_CLI_TELEMETRY_OPTOUT=1
  - export PATH=$PATH:$HOME/.dotnet/tools
  - dotnet tool install dotnet-reportgenerator-globaltool --global || echo "DRG already installed."
  script:
    - dotnet test --collect "XPlat Code Coverage" --logger "junit;MethodFormat=Class;FailureBodyFormat=Verbose"
    - reportgenerator -reports:"**/coverage.cobertura.xml" -targetdir:"." -reporttypes:"cobertura"
    - COVERAGE=$(grep -oEm 1 'line-rate="([0-9.]+)"' ./Cobertura.xml | sed -E 's/line-rate="(.*)"/\1 * 100/g' | bc)
    - echo "TOTAL_COVERAGE=$COVERAGE%"
  coverage: '/TOTAL_COVERAGE=(\d+.\d+)/'
  artifacts:
    reports:
      junit:
        - ./**/TestResults.xml
      coverage_report:
        coverage_format: cobertura
        path: ./Cobertura.xml

deploy_package:
  stage: deploy
  needs:
    - job: test_package
  rules: 
    - if: $CI_COMMIT_TAG
  image: mcr.microsoft.com/dotnet/sdk:7.0.401-alpine3.18
  script:
    - dotnet pack -c Release -o ./package /p:Version=$CI_COMMIT_TAG -p:IncludeSymbols=true -p:SymbolPackageFormat=snupkg
    - dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    - dotnet nuget push "./package/*.nupkg" --source gitlab
